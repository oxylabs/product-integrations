## 什么是Oxylabs
Oxylabs是一家全球技术公司，提供用于Web数据收集的工具和资源。

我们的IP池包括二百万+ 数据中心IP代理和一亿+ 住宅IP代理来自大概195国家。

## Oxylabs产品集成例子

 - [数据中心IP代理](https://gitee.com/oxylabs/product-integrations/tree/master/datacenter-proxies)
 - [住宅IP代理](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies)
 - [Next-Gen (新代住宅) IP代理](https://gitee.com/oxylabs/product-integrations/tree/master/next-gen-residential-proxies)
 - [数据爬虫](https://gitee.com/oxylabs/product-integrations/tree/master/real-time-crawler)

## 文档

[完整的文档](https://developers.oxylabs.io/)
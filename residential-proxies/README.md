<p align="center">
    <a href="https://oxylabs.io/"><img src="https://oxylabs.io/build/assets/images/Logo.e7281886e69d264f38d2a38c9f276d0a.svg" alt="Oxylabs logo" width="218" height="42"></a>
  </a>
</p>

<h2 align="center">
  Oxylabs住宅代理IP池
</h2>

<p align="center">
  Oxylabs提供来自世界各地的1亿+ 住宅代理IP池。可以按国家，城市或ASN进行过滤。
</p>

## 导航键

- [功能](#功能)
- [住宅代理IP池如何操作](#住宅代理IP池如何操作)
- [入门指南](#入门指南)
- [集成](#集成)
- [联系我们](#联系我们)

## 功能

- 无限线路数
- IP白名单
- 支持Sticky线路
- 公共API
- 全球覆盖 （州和城市）
- 快速度和正常操作时间
 
## 住宅代理IP池如何操作

<br> 1. 将住宅代理端点配置到爬虫
<br> 2. 对于每一个请求我们的系统会选择一个代理IP并将您的请求转发到网络中
<br> 3. 请求的网页将仅看到我们系统的代理

## 入门指南
<br> 1. [注册Oxylabs账号](https://dashboard.oxylabs.io/registration)
<br> 2. 选择 “数据中心代理” 产品。
<br> 3. 等我们的销售人员联系你。
<br> 4. 开始用了!


如何实施和开始使用住宅代理IP池的详细指南，请查看我们的[快速度入门指南。](https://oxylabs.io/blog/residential-proxies-quick-start-guide). 

Oxylabs 住宅代理IP池支持自助服务功能：入门级和专业套餐。[注册账号](https://dashboard.oxylabs.io/registration)后，这些选项将变为可用。

如何操作我们的自助服务的详细信息，请务必查看[此分步指南。](https://oxylabs.io/blog/fast-checkout-guide).

## 集成

- [C#](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/CSharp)
- [Java](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/Java)
- [Node.js](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/Nodejs)
- [PHP](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/PHP)
- [Python](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/Python)
- [Ruby](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/Ruby)
- [Shell](https://gitee.com/oxylabs/product-integrations/tree/master/residential-proxies/Shell)



## 联系我们
邮箱： hello@oxylabs.io
<br><a href="https://oxylabs.drift.click/oxybot">在线客服</a>

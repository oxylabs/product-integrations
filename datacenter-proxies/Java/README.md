文档: https://developers.oxylabs.io/datacenter-proxies/

# 要求

- Java 1.8+

此示例不需要任何第三方依赖

# 用法

编译（代码）:
```bash
$ javac ProxyRequest.java
```

 运行（软件）:
```bash
$ java ProxyRequest
```
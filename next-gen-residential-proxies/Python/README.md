文档: https://developers.oxylabs.io/next-gen-residential-proxies/

# 要求

- Python3
- requests
- prettyprinter

# 安装（软件）
使用 pip:

```bash
$ pip install requests prettyprinter
```

# 用法

运行 （软件）:
```bash
$ python3 python.py
```
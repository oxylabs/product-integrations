文档: https://developers.oxylabs.io/next-gen-residential-proxies/

# 要求

- NodeJS
- request
- request-promise

# 安装（软件）
使用 npm:

```bash
$ npm install request request-promise
```

# 用法

运行 （软件）:
```bash
$ node node.js
```
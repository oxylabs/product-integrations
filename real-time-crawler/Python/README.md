文档: https://developers.oxylabs.io/real-time-crawler/

# 要求

- Python 3.1+
- requests
- prettyprinter
- sanic

# 安装（软件）
使用 pip:

```bash
$ pip install requests prettyprinter sanic
```

# 用法

```bash
$ python SingleQuery.py
```